FROM alpine:3.9

RUN apk add --update --no-cache \
    openssh=7.9_p1-r6 \
    sshpass=1.06-r0 \
    bash=4.4.19-r1 \
    rsync=3.1.3-r1

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.5.0/common.sh

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
